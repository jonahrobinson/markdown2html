#!/bin/bash

# Depends on package(s): 
# - markdown

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Usage   : markdown2html [OPTION]... [BASEFILE]... [SOURCEDIR]... ([DESTINATIONDIR]...)"
            echo "Function: Convert .md files (in SOURCEDIR) to .html files"
            echo "Example : markdown2html base.html ."
            echo " "
            echo "Param 1 : html BASEFILE, used to generate html files from the converted markdown contents"
            echo "Param 2 : SOURCEDIR containing markdown files"
            echo "Param 3 : (Optional) DESTINATIONDIR for the output html files to be stored"
            echo " "
            echo "Options :"
            echo "  -h, --help : display this help text and exit"
            echo " "
            exit 0
            ;;
        *)
            break
            ;;
    esac
done

# The base file contains headers and fields which are generic to all html files.
# This file MUST contain the comment: <!-- REPLACE WITH MARKDOWN -->
BASE_FILE=${1:-""}

# The source directory is the folder in which to recursively convert markdown2html.
SOURCE_DIRECTORY=${2:-""}

# The destination directory for the output html files.
DESTINATION_DIRECTORY=${3:-""}

function generate_html() {
    local FILE="${1}"
    local BASE_FILE="${2}"
    local DESTINATION_DIRECTORY="${3}"

    local OUTPUT_FILE=$FILE

    # If the optional DESTINATION_DIRECTORY is populated use it.
    if [[ $DESTINATION_DIRECTORY != "" ]]; then
        FILE_BASE_NAME=`basename $FILE`
        OUTPUT_FILE=$DESTINATION_DIRECTORY$FILE_BASE_NAME
    fi

    # Remove file if exists.
    if [[ -f "$OUTPUT_FILE.html" ]]; then
        rm $OUTPUT_FILE.html
    fi

    # Create file.
    touch $OUTPUT_FILE.html

    IFS='' 
    cat $BASE_FILE | while read baseline
    do
        # Find line '<!-- REPLACE WITH MARKDOWN -->' and replace with html generated from markdown.
        if echo ${baseline} | grep -q '<!-- REPLACE WITH MARKDOWN -->'; then
            IFS='' 
            markdown $FILE.md | while read markdownline
            do
                echo $markdownline >> $OUTPUT_FILE.html
            done
        else
            echo $baseline >> $OUTPUT_FILE.html
        fi
    done
    echo "$OUTPUT_FILE.html has been generated"
}

VALID_PARAMETERS=true

# Determine whether $BASE points to an actual file.
if [[ ! -f "$BASE_FILE" ]]; then
    VALID_PARAMETERS=false
    if [[ $BASE_FILE == "" ]]; then
        echo "Please enter a valid base file as parameter 1"
    else
        echo "Error: $BASE_FILE not valid. Please enter a valid base file as parameter 1"
    fi
fi

# Determine whether $SOURCE_DIRECTORY points to an actual directory.
if [[ ! -d "$SOURCE_DIRECTORY" ]]; then
    VALID_PARAMETERS=false
    if [[ $SOURCE_DIRECTORY == "" ]]; then
        echo "Please enter a valid source directory as parameter 2"
    else
        echo "Error: $SOURCE_DIRECTORY not valid. Please enter a valid source directory as parameter 2"
    fi
fi

# Determine whether $DESTINATION_DIRECTORY points to an actual directory.
if [[ ($DESTINATION_DIRECTORY != "") ]]; then
    if [[ ! -d "$DESTINATION_DIRECTORY" ]]; then
        VALID_PARAMETERS=false
        echo "Error: $DESTINATION_DIRECTORY not valid. (Optional) Please enter a valid destination directory as parameter 3"
    else
        # Ensure that the DESTINATION_DIRECTORY variable has a trailing /
        if [[ "${DESTINATION_DIRECTORY: -1}" != "/" ]]; then
            DESTINATION_DIRECTORY=$DESTINATION_DIRECTORY/
        fi
    fi
fi

if [[ $VALID_PARAMETERS == true ]] ; then
    # Find all .md files in the $SOURCE_DIRECTORY path, recursively.
    find $SOURCE_DIRECTORY -name "*.md" -type f | while IFS= read file
    do  
        # Avoid generating .html file for README.md
        if ! $(echo ${file} | grep -qi 'README.md'); then
            # Generate html files for all found markdown files.
            generate_html "${file%.md}" "${BASE_FILE}" "${DESTINATION_DIRECTORY}"
        fi
    done
fi