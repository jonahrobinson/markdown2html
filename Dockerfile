FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -y \
    markdown \
    locales \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Set locale to correct value.
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

COPY markdown2html.sh /usr/local/bin

# Allow the user to run `markdown2html` from anywhere.
RUN ln /usr/local/bin/markdown2html.sh /usr/local/bin/markdown2html
