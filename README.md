# markdown2html

A simple tool written in bash to convert many markdown files to html and embed the contents in another html document. Used by the project [jonahrobinson.com](https://gitlab.com/jonahrobinson/jonahrobinson.com).

This tool was designed for use with Visual Studio Code's [Developing inside a container](https://code.visualstudio.com/docs/remote/containers) feature and for GitLab's CI/CD pipeline.

## How to install

This project utilises [docker](https://www.docker.com/) and can either be ran locally inside of a docker container or as standalone bash tool. The standalone tool depends on the following packages [markdown](https://daringfireball.net/projects/markdown/).

Prefer the containerised solution, its easy to use and packages up the tool nicely.

Either pull the image from the repository.

```shell
docker pull registry.gitlab.com/jonahrobinson/markdown2html:latest
```

Or clone and build the docker image yourself.

```shell
git clone git@gitlab.com:jonahrobinson/markdown2html.git
cd markdown2html
docker-compose build
```

If you're determined to forgo using docker then the project's [dockerfile](https://gitlab.com/jonahrobinson/markdown2html/-/blob/master/Dockerfile) should provide adequate reference for the step-by-step commands.

## How to use

If you wish to use the tool on the command line, you can run the following command. 

```shell
docker run -v $(pwd):/workspace registry.gitlab.com/jonahrobinson/markdown2html:latest markdown2html /workspace /workspace/resources/base.html
```

The current directory must be mounted inside the container. This tool was designed for use with Visual Studio Code's [Developing inside a container](https://code.visualstudio.com/docs/remote/containers) feature and for GitLab's CI/CD pipeline. These systems automatically bind mount the current working directory inside the container.

Parameter 1) The directory containing markdown files to convert to html. The tool will recursively scan for `.md `files at the path, considering sub-folders.

Parameter 2) The `.html` file to use as a "base" for embedding the markdown contents. Use the line `<!-- REPLACE WITH MARKDOWN -->` to reference the replacement point.